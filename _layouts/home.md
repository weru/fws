---
layout: default
---

<section class = 'overview fulls'>
    <div class = 'overlay flex-in'>{{content}}</div>
</section>
<section class = 'blog'>
   <article>
     <div class = 'hold'>
          <ol class="post-collection flex">
            {% for post in site.posts %}
            <li class = 'child duo'>
              <h3 class = 'post-link'>
              <a href="{{ post.url | relative_url }}">{{ post.title | escape }}</a>
              </h3>
              {{ post.excerpt  | truncate: 200}} 
              <a href="{{ post.url | relative_url }}"><span class = 'mark'>Read More ...</span></a>
            </li>
            {% endfor %}
          </ol>
      </div>
  </article>
</section>
